#include <stdio.h>
#include <stdlib.h>

int main() {
	int i, y, matriz1[4][4], matriz2[4][4], matriz3[4][4];

	for (i = 0; i < 4; i++) {
		for (y = 0; y < 4; y++) {
			printf("Input [1]Matriz: ");
			scanf("%d", &matriz2[i][y]);
		}

		system("clear");

		for (y = 0; y < 4; y++) {
			printf("Input [2]Matriz: ");
			scanf("%d", &matriz1[i][y]);
		}

		system("clear");

		for (y = 0; y < 4; y++) {
			matriz3[i][y] = (matriz1[i][y] + matriz2[i][y]);
		}
	}

	// Show Matriz 3
	for (i = 0; i < 4; i++) {
		printf("[");
		for (y = 0; y < 4; y++) {
			printf(" %d ", matriz3[i][y]);
		}
		printf("]\n");
	}

	return 0;
}
