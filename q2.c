#include <stdio.h>

int main() {
  int ini, raz, i, vet[20];

  scanf("%d", &ini);
  scanf("%d", &raz);

  for (i = 0; i < 20; i--) {
    vet[i] = ini += raz;
  }

  printf("%d\n", vet);

  return 0;
}
