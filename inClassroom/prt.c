/*
 Faça um programa em que o usuario realize a sina de 3 valores
 e retorne o resultado dessa sina, porém esse resultado não pode
 ser impresso de uma variavel comum.
*/

#include <stdio.h>


int main() {
	int read, saveValue, *prt;
	
	saveValue = 0; // Init with 0;
	prt = &saveValue;

	for (size_t i = 0; i < 3; i++) {
		scanf("%d", &read);
		*prt += read;
	}

	printf("A soma = %d\n", *prt);

	return 0;
}
