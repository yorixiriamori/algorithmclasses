#include <stdio.h>

int main() {
  int val[10], i, menor = 0, maior = 0, soma = 0, mult = 1;

  for (i = 0; i < 10; i++) {
    printf("Digite um valor inteiro: ");
    scanf("%d", &val[i]);

    if (i%2 == 0) {
      soma += val[i];
    }
    else {
      mult *= val[i];
    }
  }

  menor = val[0];
  for (;i > 0; i--) {
    if (maior <= val[i-1]) {
      maior = val[i-1];
    }

    if (menor >= val[i-1]) {
      menor = val[i-1];
    }
  }

  printf("Maior: %d\n", maior);
  printf("Menor: %d\n", menor);
  printf("Soma: %d\n", soma);
  printf("Mult: %d\n", mult);
  return 0;
}
