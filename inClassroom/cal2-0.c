#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
	float entrada1, entrada2, resultado;
	int op = 1;

	do {
		printf("================ Menu ================\n");
		printf("Escolha uma opcao:\n[1] Soma\n[2] Sub\n[3] Mult\n[4] Div\n[5] Pontencia\n[6] Raiz Quad\n[7] Log na Base 10\n[0] Sair\n");

		printf("\nDigite uma opcao: ");
		scanf("%d", &op);

		// condição para sair antes da entrada dos valores
		if (op == 0) {
			printf("Bye\n");
			break;
		}

		// escolhe como vai ser a entrada
		if (op == 5) {
			printf("Digite o valor da variavel: ");
			scanf("%f", &entrada1);
			printf("Digite o valor do expoente: ");
			scanf("%f", &entrada2);
		} else if (op == 6 || op == 7) {
			printf("Digite um valor: ");
			scanf("%f", &entrada1);
		} else {
			printf("Digite o primeiro valor: ");
			scanf("%f", &entrada1);
			printf("Digite o segundo valor: ");
			scanf("%f", &entrada2);
		}
		

		switch (op) {
			case 1:
				resultado = entrada1 + entrada2;
				printf("O resultado da soma eh: %.2f\n", resultado);
				break;
			case 2:
				resultado = entrada1 - entrada2;
				printf("O resultado da sub eh: %.2f\n", resultado);
				break;
			case 3:
				resultado = entrada1 * entrada2;
				printf("O resultado da mult: %.2f\n", resultado);
				break;
			case 4:
				if (entrada2 == 0) {
					printf("A divisao nao eh possivel\n");
				} else {
					resultado = entrada1 / entrada2;
					printf("O resultado da divisao: %.2f\n", resultado);
				}
				break;
			case 5:
				resultado = pow(entrada1, entrada2);
				printf("O resultado da exponeciacao: %f\n", resultado);
				break;
			case 6:
				if (entrada1 < 0) {
					printf("Nao ha resultado\n");
				} else {
					resultado = sqrt(entrada1);
					printf("A raiz quadrada de %.2f eh: %.2f\n", entrada1, resultado);
				}
				break;
			case 7:
				if (entrada1 < 0) {
					printf("Nao pode\n");
				} else {
					resultado = log10(entrada1);
					printf("O log na base 10 de %.2f eh: %.2f\n", entrada1, resultado);
				}
				break;
			default:
				printf("Operador Invalida\n");
		}
	} while (op != 0);



	return 0;
}