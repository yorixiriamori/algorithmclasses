#include <stdio.h>

int main() {
	float entrada1, entrada2, resultado;
	char op;

	printf("Primeiro valor: ");
	scanf("%f", &entrada1);

	printf("Digite a operador [+|-|*|/]: ");
	scanf("%s", &op);

	printf("Segundo valor: ");
	scanf("%f", &entrada2);

	switch (op) {
		case '+':
			resultado = entrada1 + entrada2;
			printf("o resultado da soma: %.2f\n", resultado);
			break;
		case '-':
			resultado = entrada1 - entrada2;
			printf("o resultado da sub: %.2f\n", resultado);
			break;
		case '*':
			resultado = entrada1 * entrada2;
			printf("o resultado da mult: %.2f\n", resultado);
			break;
		case '/':
			if(entrada2 == 0) {
				printf("a divisao nao e possivel\n");
			} else{
				resultado = entrada1 / entrada2;
				printf("o resultado da divisao: %.2f\n", resultado);
			}
			break;
		default: 
			printf("Operador Invalido\n");
	}

	return 0;
}