#include <stdio.h>

int main() {
  int i, vet[10];

  for (i = 0; i < 10; i++) {
    printf("Digite o valor %d: ", i+1);
    scanf("%d", &vet[i]);
  }

  for (; i > 0; i--) {
    printf("%d,", vet[i-1]);
  }


  return 0;
}
