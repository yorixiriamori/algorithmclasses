/*
2-
Faça um programa em que o usuário
preenche uma matriz 3x4.
calcule a transposta dessa matriz em
um procedimento.
Imprima a matriz de acordo com linhas e
colunas.
*/


#include <stdio.h>


void matrizTrans(int matriz[][3]);


int main() {
  int x, y, matriz[3][4], matrizTransposta[4][3];

  for (x = 0; x < 3; x++) {
    for (y = 0; y < 4; y++) {
      printf("Matriz[%d][%d]: ", x, y);
      scanf("%d", &matriz[x][y]);

      matrizTransposta[y][x] = matriz[x][y];
    }
  }

  matrizTrans(matrizTransposta);

  return 0;
}


void matrizTrans(int matriz[][3]) {
  int x, y;

  for (x = 0; x < 4; x++) {
    printf("[");
    for (y = 0; y < 3; y++) {
      printf(" %d ", matriz[x][y]);
    }
    printf("]\n");
  }
}
