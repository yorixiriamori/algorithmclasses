/*
1-
Faça um programa em que o usuário
digite 2 valores e depois peça para ele
escolher qual operação deseja realizar.
(soma, sub, div, e mult).
--> Os cálculos devem ser feitos em função
*/

#include <stdio.h>


double soma(double numberOne, double numberTwo);
double sub(double numberOne, double numberTwo);
double div(double numberOne, double numberTwo);
double multi(double numberOne, double numberTwo);


int main() {
  double inputA, inputB, out;
  int op;

  printf("Digite o primeiro valor: ");
  scanf("%lf", &inputA);

  printf("Digite o segundo valor: ");
  scanf("%lf", &inputB);

  printf("[1] - Soma\n[2] - Sub\n[3] - Div\n[4] - Multi\nOpcao: ");
  scanf("%d", &op);

  switch (op) {
    case 1:
      out = soma(inputA, inputB);
      printf("Resultado = %.2lf\n", out);
      break;
    case 2:
      out = sub(inputA, inputB);
      printf("Resultado = %.2lf\n", out);
      break;
    case 3:
      if (inputB == 0.0) {
        printf("Resultado indefinido!\n");
      }
      else {
        out = div(inputA, inputB);
        printf("Resultado = %.2lf\n", out);
      }
      break;
    case 4:
      out = multi(inputA, inputB);
      printf("Resultado = %.2lf\n", out);
      break;
  }

  return 0;
}

double soma(double numberOne, double numberTwo) {

  return (numberOne + numberTwo);
}


double sub(double numberOne, double numberTwo) {

  return (numberOne - numberTwo);
}


double div(double numberOne, double numberTwo) {

  return (numberOne / numberTwo);
}


double multi(double numberOne, double numberTwo) {

  return (numberOne * numberTwo);
}
