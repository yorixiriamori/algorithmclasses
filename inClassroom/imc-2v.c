/*
Faça um programa que receba a altura, idade, e peso de 3 pessoas
-> Calcule o IMC dessas 3 pessoas e retorne o valor de cálculo.
-> Retorne qual o mais velho entre os 3.
-> Retorne qual o mais novo entre os 3.
-> Retorne quantos dos 3 tem 18 ou mais anos.
*/

#include <stdio.h>

int main() {
  float imc[3], altura, peso;
  int idade[3], chessus[3], velho = 0, novo = 0, contador = 0, i, pVelho = 0, pNovo = 0;

  // Recebe as informações
  for (i = 0; i < 3; i++) {
    printf("============================\n");
    printf("Pessoa %d\nDigite sua altura: ", i+1);
    scanf("%f", &altura);
    printf("Digite sua idade: ");
    scanf("%d", &idade[i]);
    printf("Digite seu peso: ");
    scanf("%f", &peso);

    imc[i] = peso / (altura * altura);

    if (idade[i] >= 18) {
      contador++;
    }
  }


  // Verifica qual as idades
  novo = idade[0];
  for (; i > 0; i--) {
    if (velho <= idade[i-1]) {
      velho = idade[i-1];
      pVelho = i;
    }
    
    if (novo >= idade[i-1]) {
      novo = idade[i-1];
      pNovo = i;
    }

  }


  printf("A pessoa mais nova eh a %d, sua idade: %d e o IMC: %.2f\n", pNovo, novo, imc[pNovo-1]);
  printf("A pessoa mais velha eh a %d, sua idade: %d e o IMC: %.2f\n", pVelho, velho, imc[pVelho-1]);
  printf("Ah %d maiores de 17\n", contador);

  return 0;
}
