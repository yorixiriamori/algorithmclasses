#include <stdio.h>

int main() {
	float peso, altura, imc;
	char sexo;

	printf("Entre com um peso: ");
	scanf("%f", &peso);

	printf("Entre com uma altura: ");
	scanf("%f", &altura);

	printf("Qual é o sexo ?\nH - Homem e M - Mulher: ");
	scanf("%s", &sexo);

	imc = peso / (altura * altura);
	printf("IMC: %.2f\n", imc);

	switch (sexo) {
		case 'H':
		case 'h':
			if (imc < 20) {
				printf("Abaixo do Normal\n");
			} else if (imc >= 20 && imc < 25) {
				printf("Normal\n");
			} else if (imc >= 25 && imc < 30) {
				printf("Obesidade Leve\n");
			} else if (imc >= 30 && imc < 40) {
				printf("Obesidade Moderada\n");
			} else {
				printf("Obesidade Morbida\n");
			}
			break;
		case 'M':
		case 'm':
			if (imc < 19) {
				printf("Abaixo do Normal\n");
			} else if (imc >= 19 && imc < 24) {
				printf("Normal\n");
			} else if (imc >= 24 && imc < 29) {
				printf("Obesidade Leve\n");
			} else if (imc >= 29 && imc < 39) {
				printf("Obesidade Moderada\n");
			} else {
				printf("Obesidade Morbida\n");
			}
			break;
		default: printf("Sexo invalido\n");

	}

	return 0;
}
