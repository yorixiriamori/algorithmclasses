/*
Faça um programa que receba 3 notas
e calcule a média e retorne qual a maior
nota e qual menor nota entre as 3.
*/


#include <stdio.h>

int main() {
  float nota[3], maior = 0, menor = 0, r = 0;
  int i;

  // Faz a leitura e guarda as notas
  for (i = 0; i < 3; i++) {
    printf("Digite o valor %d: ", i+1);
    scanf("%f", &nota[i]);

    // Soma as notas
    r += nota[i];
  }

  // Procura o maior e o menor valor
  menor = nota[0];
  for (;i > 0; i--) {

    // Verifica o maior valor
    if (maior <= nota[i-1]) {
      maior = nota[i-1];
    }

    // Verifica o menor valor
    if (menor >= nota[i-1]) {
      menor = nota[i-1];
    }
  }


  printf("O maior eh: %.2f\nO menor eh: %2.f\nA media eh: %.2f\n", maior, menor, (r / 3));

  return 0;
}
