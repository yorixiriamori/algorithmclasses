#include <stdlib.h>
#include <stdio.h>

int main() {
  float in;
  int option;

  printf("Digite o valor da temperatura em Celsius: ");
  scanf("%f", &in);

  printf("Escolha em qual voce deseja converter\n1 - Kelvin e 2 - Fahrenheit: ");
  scanf("%d", &option);

  switch(option) {
    case 1: printf("Kelvin: %.2f\n", in + 273.15); break;
    case 2: printf("Fahrenheit: %.2f\n", (in * 1.8) + 32); break;
    default: printf("Escolha invalida!\n");

  }

  return 0;
}
