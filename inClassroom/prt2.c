/*
 Faça um programa em que o usuário insira 10 valores.
 +Realize a soma desses 10 valores;
 +Depois peça ao usuario digitar um valor e compare se esse valor eciste 
 entre os 10;
 +Retorne o resultado da soma, se o valor existe e qual a posição na 
 memória o valor está;
*/

#include <stdio.h>


int main() {
	int vet[10], value, tf = 1, i, *prt, soma = 0;
	
	prt = vet;

	for (i = 0; i < 10; i++) {
		scanf("%d", &vet[i]);
		soma += vet[i];
	}

	printf("Busca: ");
	scanf("%d", &value);
	
	printf("O resultado da soma => %d\n", soma);

	for (i = 0; i < 10; i++) {
		prt++;
		if (value == vet[i]) {
			printf("O valor existe no endereco: %p\n", prt);
			tf = 0;
		}
	}

	if (tf) {
		printf("O valor nao existe\n");
	}

	return 0;
}
