#include <stdio.h>

int main() {
  int valor = 0, resultado = 0, i;

  for (i = 6; i > 0; i--) {

    printf("Digite um numero: ");
    scanf("%d", &valor);

    resultado += valor;
    printf("Valor parcial: %d\n", resultado);

  }

  printf("O resultado final eh: %d\n", resultado);

  return 0;
}
