/*
Faça um programa que receba
10 valores e calcule a soma
entre os 5 primeiros valores
e a subtração do 5 últimos.
Retorne o resultado da soma
e subtração e qual o maior valor
entre os 10.
*/

#include <stdio.h>

int main() {
  float nota[10], maior = 0, menor = 0, rSoma = 0, rSub = 0;
  int i;

  // Faz a leitura e guarda as notas
  for (i = 0; i < 10; i++) {
    printf("Digite o valor %d: ", i+1);
    scanf("%f", &nota[i]);

    // Soma as notas
    if (i < 5) {
      rSoma += nota[i];
    }
    else {
      rSub -= nota[i];
    }

  }

  // Procura o maior e o menor valor
  menor = nota[0];
  for (;i > 0; i--) {

    // Verifica o maior valor
    if (maior <= nota[i-1]) {
      maior = nota[i-1];
    }

    // Verifica o menor valor
    if (menor >= nota[i-1]) {
      menor = nota[i-1];
    }
  }


  printf("O maior eh: %.2f\nO menor eh: %.2f\nA soma eh: %.2f\nA sub eh: %.2f\n", maior, menor, rSoma, rSub);

  return 0;
}
