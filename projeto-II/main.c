#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <stdlib.h>


int verificaPosicao(char *matriz, char jogada);
void imprimeMatriz(char matriz[][3]);
int verificaJogo(char matriz[][3]);
void clear();
void muda(int *vez);


int main() {
	setlocale(LC_ALL, "portuguese");

	int i, linha, coluna, vez = 1, def = 0, cont = 9;
	char jogada[2] = {'O', 'X'}, names[2][12], matriz[3][3] = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};


	for (i = 0; i < 2; i++) {
		printf("[Limite de 12 caracteres]\nDigite o nickname do player %d: ", i + 1);
		fgets(names[i], 12, stdin);

		clear();
	}



	imprimeMatriz(matriz);

	do {
		def = 0;

		// vez = (vez != 0) ? 1 : 0;

		printf("Jogador %s\n", names[vez]);
		printf("Linha: ");
		scanf("%d", &linha);

		printf("Coluna: ");
		scanf("%d", &coluna);

		clear();

		linha--;
		coluna--;

		if (verificaPosicao(&matriz[linha][coluna], jogada[vez])) {
			muda(&vez);
			cont--;
		}

		imprimeMatriz(matriz);

		if (cont < 6) {
			if (verificaJogo(matriz) == 1) {
				printf("X ganhou\n");
				break;
			}
			else if (verificaJogo(matriz) == -1) {
				printf("O ganhou\n");
				break;
			}
			else {
				def = 1;
			}
		}


	} while(cont);


	if (def) {
		clear();
		printf("Deu velha\n");
	}

	return 0;
}


int verificaPosicao(char *matriz, char jogada) {

	if (*matriz == ' ') {
		*matriz = jogada;
		return 1;
	}

	printf("A posição esta ocupada ou não existe\n");
	return 0;
}


void imprimeMatriz(char matriz[][3]) {

	printf("\t %c | %c | %c \n\t---+---+---\n\t %c | %c | %c \n\t---+---+---\n\t %c | %c | %c \n\n\n", matriz[0][0], matriz[0][1], matriz[0][2], matriz[1][0], matriz[1][1], matriz[1][2], matriz[2][0], matriz[2][1], matriz[2][2]);
}


int verificaJogo(char matriz[][3]) {
	int i, j, cont;

// Verifica linhas
	for (i = 0; i < 3; i++) {
		cont = 0;
		for (j = 0; j < 3; j++) {
			if (matriz[i][j] == 'O') {
				cont += -1;
			}
			else if (matriz[i][j] == 'X') {
				cont += 1;
			}
			else {
				cont += 0;
			}
		}
		if (cont == 3) return 1;
		if (cont == -3) return -1;
	}

// Verifica colunas
	for (i = 0; i < 3; i++) {
		cont = 0;
		for (j = 0; j < 3; j++) {
			if (matriz[j][i] == 'O') {
				cont += -1;
			}
			else if (matriz[j][i] == 'X') {
				cont += 1;
			}
			else {
				cont += 0;
			}
		}
		if (cont == 3) return 1;
		if (cont == -3) return -1;
	}

// Verifica diagonal pricipal
	cont = 0;
	for (i = 0; i < 3; i++) {

		if (matriz[i][i] == 'O') {
			cont += -1;
		}
		else if (matriz[i][i] == 'X') {
			cont += 1;
		}
		else {
			cont += 0;
		}

		if (cont == 3) return 1;
		if (cont == -3) return -1;
	}

// Verifica diagonal secundaria
	j = 2;
	cont = 0;

	for (i = 2; i >= 0; i--) {

		if (matriz[i][j - i] == 'O') {
			cont += -1;
		}
		else if (matriz[i][j - i] == 'X') {
			cont += 1;
		}
		else {
			cont += 0;
		}
	}

	if (cont == 3) return 1;
	if (cont == -3) return -1;



	return 0;
}


void clear() {
	int cont = 100;

	while (cont--) {
		printf("\n");
	}

}


void muda(int *vez) {
	if (*vez == 1) {
		*vez = 0;
	}
	else {
		*vez = 1;
	}
}
