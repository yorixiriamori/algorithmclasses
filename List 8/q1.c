#include <stdio.h>


int func(int value) {
	
	if (value) {
		value += func(value - 1);	
	}

	return value;
}


int main() {
	int input;
	
	printf("Entre com um valor inteiro: ");
	scanf("%d", &input);
	
	printf("A soma do valor %d decomposto eh = %d\n", input, func(input));

	return 0;
}
