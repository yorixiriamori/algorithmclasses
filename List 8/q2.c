#include <stdio.h>


int func(int value) {
	
	if (value) {
		value += func(value - 1);	
	}

	return value;
}


int main() {
	int input1, input2;
	
	printf("Entre com o primeiro valor inteiro: ");
	scanf("%d", &input1);
	
	printf("Entre com o segundo valor inteiro: ");
	scanf("%d", &input2);

	printf("O resultado eh = %d\n", func(input1) + func(input2));

	return 0;
}

