#include <stdio.h>

int main() {
  int ini, raz, i, vet[20];

  printf("Digite o valor inicial: ");
  scanf("%d", &ini);
  printf("Digite o valor da razao: ");
  scanf("%d", &raz);

  for (i = 0; i < 20; i++) {
    vet[i] = ini;
		ini += raz;
  }

	// TEST
	for (i = 0; i < 20; i++) {
		printf("[%d]\n", vet[i]);
	}
	// TEST

  
  return 0;
}
