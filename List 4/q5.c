#include <stdio.h>


int main() {
	char cadastro[5][60], busca[30];
	int a, i, t, ref, info = 0, o = 0;

	printf("Cadastro do livro!\n");
	printf("Use '-' para separar o nome do livro, editora e autor\n\n");
	printf("[+]Exemplo: Hari Pota-Flipperwaldt-Capivara\n\n");

	// CADASTRO
	for (i = 0; i < 5; i++) {
		printf("Cadastro %d : ", i + 1);
		fgets(cadastro[i], 60, stdin);
		
		// Substituir o '-' para o '\n'	
		for (o = 0; o < 60; o++) {
			if (cadastro[i][o] == '-') cadastro[i][o] = '\n';
		}
	
	}
	
	
	// BUSCA
	// A busca pode ser feita pelo nome do livro,
	// editora e nome do autor
	printf("\n#Busca: ");
	fgets(busca, 30, stdin);	
	
	// Faz a busca
	for (i = 0; i < 5; i++) { // Verifica cada linha
		
		for (a = 0; a < 60; a++) { // Verifica cada coluna
			

			t = 0;
			o = a;
			while (cadastro[i][o] == busca[t]) {
				if (busca[t] == '\n') {
					info = 1;
					ref = i;
					break;
				}
				o++;
				t++;
			}
		}
	}
	

	// Usei a mesma variavel para verificar se a busca foi encontrada
	if (info) {
		
		printf("Livro: ");
		for (a = 0; cadastro[ref][a] != '\n'; a++) {
			printf("%c", cadastro[ref][a]);
		}
	} else {
		printf("404");
	}
	printf("\n");


	return 0;
}
