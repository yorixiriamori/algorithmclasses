#include <stdio.h> // 
#include <stdlib.h> //

int main() {
	int vet[50], valor, i;
	char bo = 1;

	for (i = 0; i < 50; i++) {
		vet[i] = rand() % 100;
	}

	printf("Procurar o valor: ");
	scanf("%d", &valor);

	for (; i > 0; i--) {
		if (vet[i] == valor) {
			printf("O valor %d existe, sua posicao eh: [%d]\n", valor, i-1);
			bo = 0;
		}
	}

	if (bo) {
		printf("O valor %d nao existe!\n", valor);
	}

	return 0;
}
