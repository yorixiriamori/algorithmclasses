#include <stdio.h>

int main() {
  int valor, i, menor, maior, cont = 0, vet[100];

  for (i = 0; i < 5; i++) {
    scanf("%d", &vet[i]);

    // Define um valor inicial para o menor
    if (i == 0) {
      menor = vet[i];
      maior = vet[i];
    }

    // Acha o maior e menor valor
    if (maior < vet[i]) {
      maior = vet[i];
    }

    if (menor > vet[i]) {
      menor = vet[i];
    }
  }

  printf("Digite um valor para verificar sua existencia: ");
  scanf("%d", &valor);

  // Procura o 'valor' passado
  for (; i > 0; i--) {
    if (vet[i-1] == valor) {
      cont++;
    }
  }

  // Retorna se o valor existe ou não
  if (cont > 0) {
    printf("Existe o %d, %d vezes\n", valor, cont);
  } else {
    printf("Nao existe o %d\n", valor);
  }

  printf("O maior: %d\nO menor: %d\n", maior, menor);

  return 0;
}
