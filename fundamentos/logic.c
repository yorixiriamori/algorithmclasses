#include <stdio.h>


int main(void) {
	int value1, value2;

	printf("\t\tSimulador de Portas Logicas\n");

	printf("\nDigite o valor da primeira entrada: [0 ou 1] ");
	scanf("%d", &value1);

	printf("Digite o valor de segunda entrada:  [0 ou 1] ");
	scanf("%d", &value2);

	if ((value1 == 1 || value1 == 0) && (value2 == 1 || value2 == 0)) {
		/*
		OPERADORES BITWISE
		& = AND
		| = OR
		^ = XOR
		*/
		printf("%d|%d > AND = %d\n", value1, value2, value1 & value2);
		printf("%d|%d > OR  = %d\n", value1, value2, value1 | value2);
		printf("%d|%d > XOR = %d\n", value1, value2, value1 ^ value2);
	} else {
		printf("Valor digitado é invalido!\n");
	}

	return 0;
}
