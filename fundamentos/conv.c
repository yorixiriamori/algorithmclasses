#include <stdio.h>


void binario(int ndecimal);
void hex(int ndecimal);

int main() {
	int cEntrada, entrada;
	char bin[32], c;


	printf("Entre com um numero decimal: ");
	scanf("%d", &entrada);

  binario(entrada);
  printf("\n");

  hex(entrada);
  printf("\n");

	return 0;
}


void binario(int ndecimal) {

  if (!(ndecimal < 1)) {
    binario(ndecimal / 2);
    printf("%d", ndecimal % 2);
  }

}


void hex(int ndecimal) {

  if (!(ndecimal < 1)) {
    hex(ndecimal / 16);
    switch (ndecimal) {
      case 10:
        printf("A");
        break;
      case 11:
        printf("B");
        break;
      case 12:
        printf("C");
        break;
      case 13:
        printf("D");
        break;
      case 14:
        printf("E");
        break;
      case 15:
        printf("F");
        break;
      default:
        printf("%d", ndecimal % 16);
    }
  }
}
