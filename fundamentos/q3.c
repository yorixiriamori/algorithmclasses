// Objetivo:
// Sorteia 50 numeros pseudo-aleatorios, para que o usuario tente advinhar algum.

// Entrada:
// Um valor inteiro.

// Saida:
// Se o usuario acertar é mostra que o valor existe, e sua posicao no vetor,
// caso contrario, mostra que o valor não existe.

#include <stdio.h> // Para funções de entrada e saida.
#include <stdlib.h> // Para a função rand().

int main() {
	int vet[50], valor, i; // Define o vetor que vai guardar os 50 numeros pseudo-aleatorios, define tambem a variavel que recebe a entrada, e uma variavel contador.
	short bo = 0; // Define uma variavel que vai guardar um inteiro, que vai servir de valor booleano.

	for (i = 0; i < 50; i++) { // Define uma estrutura de repetição, para navegar da primeira posição até a ultima posição do vetor.
		vet[i] = rand() % 100; // Gera e guardar os numeros pseudo-aleatorios, os numeros vão de 0 a 99.
	}

	printf("Procurar o valor: ");
	scanf("%d", &valor); // Recebe a tentativa do usuario.

	for (i = 0; i < 50; i++) { // Define uma estrutura de repetição, para navegar da primeira posição até a ultima posição do vetor.
		if (vet[i] == valor) { // Verifica se a tentativa do usuario esta presente no vetor.
			printf("O valor %d existe, sua posicao eh: [%d]\n", valor, i); // Imprime se o usuario acertou, e sua posicao.
			bo = 1; // O valor sera verdadeiro.
		}
	}

	if (!bo) { // Se a negação do valor for verdadeira.
		printf("O valor %d nao existe!\n", valor); // Imprime que o valor não existe no vetor.
	}


	return 0;
}
