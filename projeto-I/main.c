#include <stdio.h>
#include <stdlib.h>

#if defined(__MINGW32__) || defined(_MSC_VER)
#define limpar_tela() system("cls")
#else
#define limpar_tela() system("clear")
#endif


int main() {
	
	// Variaveis 'win' e 'sair' são para verificar se eh para sair
	// do loop, e indicar se o jogador ganhou ou não
	int a, i, existe, tx, sair = 0, win = 0, tentativa = 4, tamanho = 0;
	char op, palavraSecreta[30], chutePalavra[30];
	
	printf("Digite a palavra secreta: ");
	scanf("%s", palavraSecreta);

	limpar_tela(); // Definir essa macro para limpar a tela do linux e do windows

	for (i = 0; palavraSecreta[i] != '\0'; i++) tamanho++; //= i + 1; 
	
	char palavraTela[tamanho];
	for (i = 0; i < tamanho; i++) palavraTela[i] = '_';
	//palavraTela[i] = '\0'; // Acho que nao vai precisar

	do {

		existe = 0;
		if (tentativa == 0) {
			limpar_tela();
			printf("Tela: %s [%d]\n", palavraTela, tamanho);
			printf("Ja foram suas 4 tentativas\nDigite a palavra: ");
			scanf("%s", chutePalavra);
			op = '2';
			sair = 1;
		}
		else {
			voltar:
				printf("Voce tem %d chance(s)\nTela: %s [%d]\n[1] Letra\n[2] Palavra\nOpcao: ", tentativa, palavraTela, tamanho);
				scanf("%s", &op);

				printf(">>> ");
				scanf("%s", chutePalavra);
		}
		
		limpar_tela();

		tx = tamanho;
		switch (op) {

			// Verifica se a letra esta certa
			case '1':
				for (i = 0; i < tx; i++) {
					if (palavraSecreta[i] == chutePalavra[0]) {
						palavraTela[i] = chutePalavra[0];
						existe = 1;
					}
				}

				// Verificar se a palavra ja foi encontrada
				for (i = 0; palavraTela[i] == palavraSecreta[i]; i++) {
					if (palavraTela[i] == '\0' && palavraSecreta[i] == '\0') {
						existe = 1;
						win = 1;
					}
				}
				
				break;
			case '2':

				// Verifica se a palavra ta errada!
				for (i = 0; i < tx; i++) {
					if (chutePalavra != palavraTela) {
						sair = 1;
					}
				}

				// Verifica se a palavra que ele chutou esta certa
				for (i = 0; palavraSecreta[i] == chutePalavra[i]; i++) {
					if (chutePalavra[i] == '\0' && palavraSecreta[i] == '\0') {
						for (a = 0; chutePalavra[a] != '\0'; a++) {
							palavraTela[a] = palavraSecreta[a];
						}
						win = 1;
						existe = 1;
					}							
				}
				
			 break;	
			default:
				printf("Opcao Invalida!\n");
				goto voltar; // Caso o usuario digite a opcao errada ele vai da outra chance
		}	
	
		// Imprime se o usuario acertou a letra
		if (!existe) {
			if (op == '1') {
				printf("A palavra nao contem a letra '%c'\n", chutePalavra[0]);
			}
			else {
				printf("Nao eh a palavra\n");
			}
			tentativa--;
		}
		else {
			printf("Voce acertou!\n");
		}

		
	} while(sair == 0 && win == 0);

	limpar_tela();

	if (win) {
		printf("Voce ganhou, Parabens!\nA palavra secreta eh: %s\n", palavraSecreta);
	}
	else {
		printf("Voce perdeu :(\nA palavra secreta eh: %s\n", palavraSecreta);
	}

	return 0;
}
