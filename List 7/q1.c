#include <stdio.h>


void proMatriz(int matrizOne[][4], int matrizTwo[][4]);


int main() {
  int i, j, matriz1[4][4], matriz2[4][4];;

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      printf("Digite o valor inteiro da Matriz 1, posicao[%d][%d]: ", i, j);
      scanf("%d", &matriz1[i][j]);
    }
  }

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      printf("Digite o valor inteiro da Matriz 2, posicao[%d][%d]: ", i, j);
      scanf("%d", &matriz2[i][j]);
    }
  }

  proMatriz(matriz1, matriz2);

  return 0;
}


void proMatriz(int matrizOne[][4], int matrizTwo[][4]) {
  int i, count = 4, soma = 0, sub = 0, mult = 1;

  for (i = 0; i < count; i++) {
    soma += matrizOne[i][i];
    mult *= matrizTwo[i][i];
  }

  sub = mult - soma;
  printf("Resultado = %d\n", sub);
}
