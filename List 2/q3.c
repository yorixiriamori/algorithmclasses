#include <stdlib.h>
#include <stdio.h>

int main() {
	int valor;

	printf("Digite um valor inteiro: ");
	scanf("%d", &valor);

	if (valor < 3) {
		printf("Menor que 3\n");
	} else if (valor > 25) {
		printf("Maior que 25\n");
	} else {
		printf("O valor esta entre 3 e 25\n");
	}

	return 0;
}