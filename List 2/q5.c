#include <stdlib.h>
#include <stdio.h>

int main() {
	float casa, emprestimo, salario, parcelas;
	int anos;

	printf("Valor da casa: ");
	scanf("%f", &casa);

	printf("Valor do salario: ");
	scanf("%f", &salario);

	printf("Valor do emprestimo: ");
	scanf("%f", &emprestimo);

	printf("Quantos anos sera quitado o pagamento do emprestimo: ");
	scanf("%d", &anos);

	parcelas = emprestimo / (anos * 12);

	if ((salario * 0.3) >= parcelas) {
		printf("Emprestimo aprovado!\n");
	} else {
		printf("Emprestimo negado!\n");
	}

	printf("Valor da parcela: %f\n", parcelas);


	return 0;
}