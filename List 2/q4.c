#include <stdlib.h>
#include <stdio.h>

int main() {
	float valor1, valor2, resul;

	printf("Digite um valor: ");
	scanf("%f", &valor1);

	printf("Digite um outro valor: ");
	scanf("%f", &valor2);

	resul = valor1 + valor2;

	if (resul > 20) {
		printf(">%.2f\n", resul + 8);
	} else {
		printf(">%.2f\n", resul - 5);
	}

	return 0;
}