#include <stdlib.h>
#include <stdio.h>

int main() {
	float valor1, valor2, valor3, valorMenor, valorMaior, valorX;

	printf("Digite o primeiro valor: ");
	scanf("%f", &valor1);

	printf("Digite o segundo valor: ");
	scanf("%f", &valor2);

	printf("Digite o terceiro valor: ");
	scanf("%f", &valor3);


	if (valor1 >= valor2 && valor1 >= valor3) {
		valorMaior = valor1;
	} else if (valor2 >= valor1 && valor2 >= valor3) {
		valorMaior = valor2;
	} else {
		valorMaior = valor3;
	}


	if (valor1 <= valor2 && valor1 <= valor3) {
		valorMenor = valor1;
	} else if (valor2 <= valor1 && valor2 <= valor3) {
		valorMenor = valor2;
	} else {
		valorMenor = valor3;
	}


	if (valorMaior != valor1 && valorMenor != valor1) {
		valorX = valor1;
	} else if (valorMaior != valor2 && valorMenor != valor2) {
		valorX = valor2;
	} else {
		valorX = valor3;
	}

	printf("O maior eh: %.1f\nO menor eh: %.1f\n", valorMaior, valorMenor);	
	printf("Na ordem: %.1f,  %.1f, %.1f\n", valorMaior, valorX, valorMenor);

	return 0;
}
