#include <stdlib.h>
#include <stdio.h>

int main() {
	float salario;
	char cargo;

	printf("Coronel -> (C)\nMajor   -> (M)\nTenente -> (T)\nDigite seu cargo: ");
	scanf("%c", &cargo);

	printf("Digite seu salario: ");
	scanf("%f", &salario);

	if (cargo == 'C' || cargo == 'c') {
		printf("Coronel, o valor do seu aumento eh R$%.2f\n", salario * 0.6);
	} else if (cargo == 'M' || cargo == 'm') {
		printf("Marjor, o valor do seu aumento eh R$%.2f\n", salario * 0.4);
	} else if (cargo == 'T' || cargo == 't') {
		printf("Tenente, o valor do seu aumento eh R$%.2f\n", salario * 0.2);
	} else {
		printf("Cargo invalido!\n");
	}


	return 0;
}