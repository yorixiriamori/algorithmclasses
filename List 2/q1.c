#include <stdlib.h>
#include <stdio.h>

int main() {
	int valor;

	printf("Digite um valor: ");
	scanf("%d", &valor);
	printf("%d\n", valor);
	if(valor >= 100 && valor <= 200){
		printf("O valor digitado se encontra entre a faixa de 100 e 200\n");
	} else {
		printf("O valor digitado não se encontra entre a faixa de 100 e 200\n");
	}

	return 0;
}