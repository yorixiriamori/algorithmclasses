#include <stdio.h>

int main() {
	float value; 
	int cont = 0;

	printf("Digite um valor: ");
	scanf("%f", &value);
	
	do {
		value = value / 2;
		cont++;

	} while(value >= 1);

	printf("Resultado: %.2f\nNumeros de divisoes: %d\n", value, cont);

	return 0;
}
