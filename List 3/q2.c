#include <stdio.h>

int main() {
	char in;
	int imp = 0, par = 0, value;

	printf("Deseja inserir um valor: ");
	scanf("%c", &in);

	while (in == 'S' || in == 's') {

		printf("Digite um valor: ");
		scanf("%d", &value);

		if (value%2 == 0) {
			par++;
		} else {
			imp++;
		}

		printf("Deseja inserir um valor: ");
		scanf("%c", &in);
	}

	printf("Par:%d\nImpar:%d\n", par, imp);


	return 0;
}
