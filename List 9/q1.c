/*
Faça um programa em que o usuário preencha uma matriz quadrada de ordem 4; (check)
-Em uma função calcule a soma da diagonal principal. (check)
-Como resultado dessa soma calcule em uma função recursiva o fatorial. (check)
-Em um procedimento calcule o resultado do fatorial subtraido pelo resultado
 da soma da diagonal principal. (check)
-Imprima também neste mesmo procedimento se o resultado da subtração anterior
 é um valor par ou impar. (check)
-Imprima também nesse procedimento se o resultado da subtração anterior esta
 entre o intervalor de 50 a 100. (check)
-Numa função calcule a transposta da matriz preenchida. (check)
-Numa função calcule a soma de todos os elementos de matriz. (check)
-Em um procedimento peça ao usuário digitar um valor para ser buscado na matriz,
 se o valor existir diga em que posição esse valor esta. (check)
*/

#include <stdio.h>


int somaDiagonal(int matriz[4][4]);
int fatorial(int num);
void proVerifica(int num1, int num2);
void transposta(int matriz[4][4]);
int somaMatriz(int maitrz[4][4]);
void buscaValor(int matriz[4][4]);


int main() {
  int tam = 4, i, j, fat, somadi, somatriz, matriz[tam][tam];


  for (i = 0; i < tam; i++) {
    for (j = 0; j < tam; j++) {
      printf("Digite o valor inteiro positivo para a matriz, posicao[%d][%d]: ", i, j);
      scanf("%d", &matriz[i][j]);
    }
  }


  // Imprime a matriz
  for (i = 0; i < 4; i++) {
    printf("[");
    for (j = 0; j < 4; j++) {
      printf(" %d ", matriz[i][j]);
    }
    printf("]\n");
  }

  somadi = somaDiagonal(matriz); // Define o valor da soma da diagonal principal da matriz;
  fat = fatorial(somadi); // Define o valor do fatorial;

  proVerifica(fat, somadi); // Calcula e imprime o resultado da subtração, se o valor eh par ou impar, e se ele ta entre 50 e 100;
  transposta(matriz); // Imprime a matriz transposta;

  somatriz = somaMatriz(matriz); // Define o valor da soma da matriz;

  printf("[+]A soma dos valores da matriz eh: %d\n", somatriz); // Imprime os valor da soma;

  buscaValor(matriz); // Busca um valor na matriz;

  return 0;
}


int somaDiagonal(int matriz[4][4]) {
  int i, r = 0;

  for (i = 0; i < 4; i++) {
    r += matriz[i][i];
  }

  return r;
}


int fatorial(int num) {

  if (num > 1) {
    num *= fatorial(num - 1);
  }

  return num;
}


void proVerifica(int num1, int num2) {
  int r;

  r = num1 - num2;
  // printf("Resultado da subtracao  => %.2d\n", r);

  if (r == 0) {
    printf("[+]Eh nulo\n");
  }
  else if (r%2 == 0) {
    printf("[+]Eh par\n");
  }
  else {
    printf("[+]Eh impar\n");
  }

  if (r >= 50 && r <= 100) {
    printf("[+]Esta entre o intervalo de 50 a 100\n");
  }
  else {
    printf("[+]Nao esta entre o intervalo de 50 a 100\n");
  }
}


void transposta(int matriz[4][4]) {
  int i, j;

  printf("[+]Matriz Transposta:\n");

  for (i = 0; i < 4; i++) {
    printf("[");
    for (j = 0; j < 4; j++) {
      printf(" %d ", matriz[j][i]);
    }
    printf("]\n");
  }

}


int somaMatriz(int matriz[4][4]) {
  int i, j, soma = 0;

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      soma += matriz[i][j];
    }
  }

  return soma;
}


void buscaValor(int matriz[4][4]) {
  int i, j, valor, booleano = 0;

  printf("\nDigite um valor inteiro positivo para buscar na matriz: ");
  scanf("%d", &valor);

  for (i = 0; i < 4; i++) {
    for (j = 0; j < 4; j++) {
      if (valor == matriz[i][j]) {
        printf("O valor %d existe na posicao [%d][%d]\n", valor, i, j);
        booleano = 1;
      }
    }
  }

  if (!booleano) {
    printf("O valor %d nao encontrado!\n", valor);
  }

}
